from fastapi import FastAPI, Request, Form, Cookie, Depends
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from typing import Optional

from quotes import QUOTES
import random
from dotenv import load_dotenv
import os

load_dotenv()
app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

COOKIE_KEY = "Administrator_C00kie"
COOKIE_VALUE = os.getenv('ADMIN_COOKIE')


async def is_admin(cookie: Optional[str] = Cookie(None, alias=COOKIE_KEY)) -> bool:
    if cookie is None:
        return False
    return cookie == COOKIE_VALUE


@app.get("/", response_class=HTMLResponse)
async def home(request: Request, admin: bool = Depends(is_admin)):
    context = {
        "request": request,
        "quote": random.choice(QUOTES),
        "is_admin": admin
    }
    return templates.TemplateResponse("index.html", context)


@app.get("/admin", response_class=HTMLResponse)
async def admin(request: Request, admin: bool = Depends(is_admin)):
    context = {
        "request": request,
        "is_admin": admin,
    }
    return templates.TemplateResponse("admin.html", context)


@app.get("/login", response_class=HTMLResponse)
async def login(request: Request, error: Optional[bool] = False, logged_in: bool = Depends(is_admin)):
    if logged_in:
        return RedirectResponse("/")
    context = {
        "request": request,
        "error": error
    }
    return templates.TemplateResponse("login.html", context)


@app.post("/login", response_class=RedirectResponse)
async def login_post():
    return RedirectResponse("/login?error=1", status_code=303)


@app.get("/logout", response_class=RedirectResponse)
async def logout(admin: bool = Depends(is_admin)):
    if admin:
        response = RedirectResponse("/")
        response.delete_cookie(key=COOKIE_KEY)
        return response
    else:
        return RedirectResponse("/")
