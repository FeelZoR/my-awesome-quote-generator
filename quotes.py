QUOTES = [
    "Dans le doute, reboot",
    "Vous avez essayé de débrancher puis rebrancher ?",
    "Toujours marteler CTRL+C puis doucement appuyer sur CTRL+V",
    "Mon IDE ne sauvegarde que si j'appuie 10 fois sur CTRL+S",
    "Il ne faut pas pousser son ordi dans les orties...",
    "Sauvez des manchots, utilisez Windows",
    "This website is sentient",
    "Je ne t'<a href='https://www.youtube.com/watch?v=dQw4w9WgXcQ'>abandonnerai jamais</a>",
    "1+1=10",
    "Si ça rate, formate",
    "Ce n'est pas un bug, c'est une feature !",
    "On ne met pas en prod un vendredi... sauf si on n'aime pas ses collègues !",
    "Les logiciels et les cathédrales sont très similaires : d'abord on les construit, ensuite on prie ! (<a href='https://simonecarletti.com/quotes/52a5eb7c0d52b0753b00000a/'>source</a>)"
]
